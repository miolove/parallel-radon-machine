import time
import pickle
import os
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from matplotlib import cm
from framework.logger import log

CENTRAL = 'central'
TIMED = 'timed'
COLORS = ['#000000','#FF0000','#0000FF','#00FF00','#8A2BE2','#7FFF00','#FFD700','#FF7F50','#FF69B4','#20B2AA','#DA70D6','#AFEEEE']
MAX_TABLE_ELEMENT_LENGTH = 24

class Output:
    def __init__(self, outDir, timeFormat = '%d.%m.%Y %H:%M:%S'):
        self.outDir = outDir
        self.timeFormat = timeFormat
        
    def writeExperimentSummary(self, scores, durations, datasetInfo, methods, metrics, startTime, endTime, nFolds):
        stOut = "Experiment started at %s, ended at %s" % (time.strftime(self.timeFormat, time.localtime(startTime)), time.strftime(self.timeFormat, time.localtime(endTime)))+"\n\n"
        stOut += "Experiments conducted using "+str(nFolds)+"-fold cross-validation.\n\n"
        for metric in metrics:
            stOut += self.createTableForMetric(metric, scores, durations, datasetInfo, methods)
            
        f = open(self.outDir + "/summary.txt", "w")
        f.write(stOut)
        f.close()
                
    def createTableForMetric(self, metric, scores, durations, datasetInfo, methodsFromExp):
        methods = []
        for method in methodsFromExp:
            methods.append(str(method))
        for method in durations:
            if method not in methods:
                methods.append(method)
        stOut = "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(method)[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(2*MAX_TABLE_ELEMENT_LENGTH) + "\t\t" + "\t"
        stOut += "\n" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        stOut = stOut[:-2] + "\n"
        for dataset in datasetInfo:
            dataInfo = str(dataset)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t" + ("["+str(datasetInfo[dataset]['task'])+"]")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"+("("+str(datasetInfo[dataset]['N'])+"x"+str(datasetInfo[dataset]['D'])+")")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+(":"+str(datasetInfo[dataset]['classBalance']))[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)
            stOut += dataInfo+"\t"
            for method in methods:
                stOut += str(scores[str(method)][dataset][metric])[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+str(durations[str(method)][dataset])[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            stOut += "\n"
        return stOut
    
    def writeParallelExperimentSummary(self, scores, durations, datasetInfo, methods, learners, radonLearners, metrics, startTime, endTime, folds, actualNForTimedCentralExps):
        methodNames = []
        for key in durations:
            methodNames.append(str(key))
        possParamsHN, possParamsTimed = self.calcAveragesParallelSetting(methodNames, scores, durations, metrics, folds) 
        stOut = "Experiment started at %s, ended at %s" % (time.strftime(self.timeFormat, time.localtime(startTime)), time.strftime(self.timeFormat, time.localtime(endTime)))+"\n\n"
        stOut += "Experiments conducted using "+str(folds)+"-fold cross-validation.\n\n"        
        for metric in metrics:
            stOut += self.createTableForMetricParallelSetting(metric, scores, durations, datasetInfo, learners, radonLearners, possParamsHN, possParamsTimed, actualNForTimedCentralExps)      
            stOut += "\n"
        f = open(self.outDir + "/summary.txt", "w")
        f.write(stOut)
        f.close()
        if not os.path.exists(self.outDir+"/results"):
            os.makedirs(self.outDir+"/results")
        pickle.dump(scores, open(self.outDir + "/results/scores.p", 'wb'))
        pickle.dump(durations, open(self.outDir + "/results/durations.p", 'wb'))
        pickle.dump(datasetInfo, open(self.outDir + "/results/datasetInfo.p", 'wb'))
        pickle.dump(actualNForTimedCentralExps, open(self.outDir + "/results/actualNForTimedCentralExps.p", 'wb'))
        learnerParams = {}
        for learner in learners:
            learnerParams[learner] = learner.getInitParams()
        pickle.dump(learnerParams, open(self.outDir + "/results/learnerParams.p", 'wb'))
        
    def calcAveragesParallelSetting(self, methods, scores, durations, metrics, folds):
        possParamsHN = []
        possParamsTimed = []
        for learner in methods:
            for dataset in scores[str(learner)].keys():
                for metric in metrics:
                    for key in scores[str(learner)][dataset][metric].keys():
                        if key != CENTRAL and TIMED not in key and key not in possParamsHN:
                            possParamsHN.append(key)
                        if TIMED in key:
                            possParamsTimed.append(key)
                        avg = 0.0
                        std = 0.0
                        for i in xrange(folds):
                            score = scores[str(learner)][dataset][metric][key][i]
                            if not isinstance(score, float):
                                log( score)
                                score = 0.0
                            avg += score / float(folds)
                        for i in xrange(folds):
                            if not isinstance(score, float):
                                log( score)
                                score = 0.0
                            std += (scores[str(learner)][dataset][metric][key][i] - avg)**2
                        std /= float(folds-1)
                        std = sqrt(std)
                        scores[str(learner)][dataset][metric][key]['avg'] = avg
                        scores[str(learner)][dataset][metric][key]['std'] = std     
                for key in durations[str(learner)][dataset].keys():                     
                    avgTrain = 0.0
                    stdTrain = 0.0
                    avgSync  = 0.0
                    stdSync  = 0.0
                    avgTest  = 0.0
                    stdTest  = 0.0
                    avgTotal = 0.0
                    stdTotal = 0.0
                    for i in xrange(folds):
                        avgTrain += durations[str(learner)][dataset][key][i]['train'] / float(folds)
                        avgSync  += durations[str(learner)][dataset][key][i]['sync'] / float(folds)
                        avgTest  += durations[str(learner)][dataset][key][i]['test'] / float(folds)
                        avgTotal += durations[str(learner)][dataset][key][i]['total'] / float(folds)
                    for i in xrange(folds):
                        stdTrain += (durations[str(learner)][dataset][key][i]['train']-avgTrain)**2 / float(folds-1)
                        stdSync  += (durations[str(learner)][dataset][key][i]['sync']-avgSync)**2 / float(folds-1)
                        stdTest  += (durations[str(learner)][dataset][key][i]['test']-avgTest)**2 / float(folds-1)
                        stdTotal += (durations[str(learner)][dataset][key][i]['total']-avgTotal)**2 / float(folds-1)
                    durations[str(learner)][dataset][key]['avg'] = {}
                    durations[str(learner)][dataset][key]['std'] = {}
                    durations[str(learner)][dataset][key]['avg']['train'] = avgTrain
                    durations[str(learner)][dataset][key]['avg']['sync']  = avgSync
                    durations[str(learner)][dataset][key]['avg']['test']  = avgTest
                    durations[str(learner)][dataset][key]['avg']['total'] = avgTotal
                    durations[str(learner)][dataset][key]['std']['train'] = stdTrain
                    durations[str(learner)][dataset][key]['std']['sync']  = stdSync
                    durations[str(learner)][dataset][key]['std']['test']  = stdTest
                    durations[str(learner)][dataset][key]['std']['total'] = stdTotal
        return possParamsHN, possParamsTimed
            
    def createTableForMetricParallelSetting(self, metric, scores, durations, datasetInfo, learners, parallelLearners, possParamsHN, possParamsTimed, actualNForTimedCentralExps):
        stOut = "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for learner in learners:
            stOut += str(learner)[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(2*MAX_TABLE_ELEMENT_LENGTH) + "\t\t" + "\t"
        for parallel in parallelLearners:
            for param in possParamsHN:
                stOut += (str(parallel)+" "+str(param))[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(2*MAX_TABLE_ELEMENT_LENGTH) + "\t\t" + "\t"
        for learner in learners:
            for param in possParamsTimed:
                stOut += (str(learner)+str(param))[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(2*MAX_TABLE_ELEMENT_LENGTH) + "\t\t" + "\t"
        stOut += "\n" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for learner in learners:
            stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for parallel in parallelLearners:
            for param in possParamsHN:
                stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for learner in learners:
            for param in possParamsTimed:
                stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        stOut = stOut[:-2] + "\n"
        for dataset in datasetInfo:
            dataInfo = str(dataset)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t" + ("["+str(datasetInfo[dataset]['task'])+"]")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"+("("+str(datasetInfo[dataset]['N'])+"x"+str(datasetInfo[dataset]['D'])+")")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)
            stOut += dataInfo+"\t"
            for learner in learners:
                stOut += (str(scores[str(learner)][dataset][metric][CENTRAL]['avg'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(scores[str(learner)][dataset][metric][CENTRAL]['std'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+(str(durations[str(learner)][dataset][CENTRAL]['avg']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(durations[str(learner)][dataset][CENTRAL]['std']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            for parallel in parallelLearners:
                for param in possParamsHN:
                    if param in scores[str(parallel)][dataset][metric].keys():
                        stOut += (str(scores[str(parallel)][dataset][metric][param]['avg'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(scores[str(parallel)][dataset][metric][param]['std'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+(str(durations[str(parallel)][dataset][param]['avg']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(durations[str(parallel)][dataset][param]['std']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
                    else:
                        stOut += ('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            for learner in learners:
                for param in possParamsTimed:
                    if param in scores[str(learner)][dataset][metric].keys():
                        stOut += (str(scores[str(learner)][dataset][metric][param]['avg'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(scores[str(learner)][dataset][metric][param]['std'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+(str(durations[str(learner)][dataset][param]['avg']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(durations[str(learner)][dataset][param]['std']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
                    else:
                        stOut += ('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            stOut += "\n"
        return stOut
    
    def writeOldParallelExperimentSummary(self, scores, durations, datasetInfo, methods, metrics, startTime, endTime, folds):
        possHs = []
        possNs = []
        for method in methods:
            for dataset in scores[str(method)].keys():
                for metric in metrics:
                    possNs = list(set(possNs + scores[str(method)][dataset][metric].keys()))              
                    for n in scores[str(method)][dataset][metric].keys():
                        possHs = list(set(possHs + scores[str(method)][dataset][metric][n].keys()))              
                        for h in scores[str(method)][dataset][metric][n].keys():                                      
                            avg = 0.0
                            std = 0.0
                            for i in xrange(folds):
                                avg += scores[str(method)][dataset][metric][n][h][i] / float(folds)
                            for i in xrange(folds):
                                std += (scores[str(method)][dataset][metric][n][h][i] - avg)**2
                            std /= float(folds-1)
                            std = sqrt(std)
                            scores[str(method)][dataset][metric][n][h]['avg'] = avg
                            scores[str(method)][dataset][metric][n][h]['std'] = std                            
                for n in durations[str(method)][dataset].keys():
                    for h in durations[str(method)][dataset][n].keys():
                        avgTrain = 0.0
                        stdTrain = 0.0
                        avgSync  = 0.0
                        stdSync  = 0.0
                        avgTest  = 0.0
                        stdTest  = 0.0
                        avgTotal = 0.0
                        stdTotal = 0.0
                        for i in xrange(folds):
                            avgTrain += durations[str(method)][dataset][n][h][i]['train'] / float(folds)
                            avgSync  += durations[str(method)][dataset][n][h][i]['sync'] / float(folds)
                            avgTest  += durations[str(method)][dataset][n][h][i]['test'] / float(folds)
                            avgTotal += durations[str(method)][dataset][n][h][i]['total'] / float(folds)
                        for i in xrange(folds):
                            stdTrain += (durations[str(method)][dataset][n][h][i]['train']-avgTrain)**2 / float(folds-1)
                            stdSync  += (durations[str(method)][dataset][n][h][i]['sync']-avgSync)**2 / float(folds-1)
                            stdTest  += (durations[str(method)][dataset][n][h][i]['test']-avgTest)**2 / float(folds-1)
                            stdTotal += (durations[str(method)][dataset][n][h][i]['total']-avgTotal)**2 / float(folds-1)
                        durations[str(method)][dataset][n][h]['avg'] = {}
                        durations[str(method)][dataset][n][h]['std'] = {}
                        durations[str(method)][dataset][n][h]['avg']['train'] = avgTrain
                        durations[str(method)][dataset][n][h]['avg']['sync']  = avgSync
                        durations[str(method)][dataset][n][h]['avg']['test']  = avgTest
                        durations[str(method)][dataset][n][h]['avg']['total'] = avgTotal
                        durations[str(method)][dataset][n][h]['std']['train'] = stdTrain
                        durations[str(method)][dataset][n][h]['std']['sync']  = stdSync
                        durations[str(method)][dataset][n][h]['std']['test']  = stdTest
                        durations[str(method)][dataset][n][h]['std']['total'] = stdTotal
        stOut = "Experiment started at %s, ended at %s" % (time.strftime(self.timeFormat, time.localtime(startTime)), time.strftime(self.timeFormat, time.localtime(endTime)))+"\n\n"
        stOut += "Experiments conducted using "+str(folds)+"-fold cross-validation.\n\n"
        for n in sorted(possNs):
            stOut += "n: "+str(n)+"\n"
            for h in sorted(possHs):
                stOut += "h: "+str(h)+" c: "+str(2.**h)+"\n"
                for metric in metrics:
                    stOut += self.createTableForMetricOldParallelSetting(metric, scores, durations, datasetInfo, methods, h, n)      
                    stOut += "\n"
        f = open(self.outDir + "/summary.txt", "w")
        f.write(stOut)
        f.close()
        if not os.path.exists(self.outDir+"/results"):
            os.makedirs(self.outDir+"/results")
        pickle.dump(scores, open(self.outDir + "/results/scores.p", 'wb'))
        pickle.dump(durations, open(self.outDir + "/results/durations.p", 'wb'))
        pickle.dump(datasetInfo, open(self.outDir + "/results/datasetInfo.p", 'wb'))
        
        
    def createTableForMetricOldParallelSetting(self, metric, scores, durations, datasetInfo, methods, h, n):
        stOut = "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(method)[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(2*MAX_TABLE_ELEMENT_LENGTH) + "\t\t" + "\t"
        stOut += "\n" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        stOut = stOut[:-2] + "\n"
        for dataset in datasetInfo:
            dataInfo = str(dataset)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t" + ("["+str(datasetInfo[dataset]['task'])+"]")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"+("("+str(datasetInfo[dataset]['N'])+"x"+str(datasetInfo[dataset]['D'])+")")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)
            stOut += dataInfo+"\t"
            for method in methods:
                if n in scores[str(method)][dataset][metric] and h in scores[str(method)][dataset][metric][n]:
                    stOut += (str(scores[str(method)][dataset][metric][n][h]['avg'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(scores[str(method)][dataset][metric][n][h]['std'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+(str(durations[str(method)][dataset][n][h]['avg']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+str(durations[str(method)][dataset][n][h]['std']['total'])[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
                else:
                    stOut += ('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+('--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+"("+'--------------'[0:(MAX_TABLE_ELEMENT_LENGTH/2-1)]+")").ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            stOut += "\n"
        return stOut
    
    def createResultPlotterFile(self):
        stOut = "import pickle\nfrom framework.output import ResultPlotter\ndurations = pickle.load(open(\"results/durations.p\", \'r\'))\nscores    = pickle.load(open(\"results/scores.p\", \'r\'))\ndatasetInfo    = pickle.load(open(\"results/datasetInfo.p\", \'r\'))\n\nplotter = ResultPlotter()\nfor metric in scores[scores.keys()[0]][scores[scores.keys()[0]].keys()[0]]:\n    plotter.plotMetricRadonVsCentralAllDatasets(scores, durations, metric)\n    plotter.plotDurationRadonVsCentralAllDatasets(scores, durations, metric)\n    plotter.plotMetricAndDurationRadonVsCentralAllDatasets(scores, durations, metric)\n    plotter.plotMetricAndDurationHistogramAllDatasets(scores, durations, metric)\nfor dataset in scores[scores.keys()[0]]:\n    for metric in scores[scores.keys()[0]][dataset]:\n        plotter.plotTheBigPicture(scores, durations, metric, dataset, datasetInfo)\n        plotter.plotSpeedupVsAccuracy(scores, durations, metric, dataset, datasetInfo)"
           
        f = open(self.outDir + "/plotResults.py", "w")
        f.write(stOut)
        f.close()
        
class ResultPlotter():
    def __init__(self):
        if not os.path.exists("charts/"):
            os.makedirs("charts/")
     
    def plotTheBigPicture(self, scores, durations, metric, dataset, datasetInfo):        
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        iColor = 0      
        N = datasetInfo[dataset]['N']
        for method in scores:    
            X = []
            Y = []
            Xerr = []
            Yerr = []        
            for key in scores[method][dataset][metric]:
                if key == CENTRAL:
                    n = N
                    h = 0
                else:
                    if not TIMED in key: 
                        n = key[0]
                        h = key[1]
                    else:
                        vals = key.replace(TIMED,'').replace('(','').replace(')','').split(',')
                        n = int(vals[0])
                        h = int(vals[1])
                score       = scores[method][dataset][metric][key]['avg']
                scoreErr    = scores[method][dataset][metric][key]['std']
                dur         = durations[method][dataset][key]['avg']['total']
                durErr      = durations[method][dataset][key]['std']['total']
                X.append(dur)
                Xerr.append(durErr)
                Y.append(score)
                Yerr.append(scoreErr)                            
            ax.errorbar(X, Y, xerr=Xerr, yerr=Yerr, ls='', marker='o', color=COLORS[iColor], label = method)
            for i in xrange(len(X)):
                key = scores[method][dataset][metric].keys()[i]
                if key == CENTRAL:
                    n = datasetInfo[dataset]['N']
                    h = 0
                    c = 1
                else:
                    if not TIMED in key: 
                        n = key[0]
                        h = key[1]
                        c = N/n
                    else:
                        vals = key.replace(TIMED,'').replace('(','').replace(')','').split(',')
                        n = int(vals[0])
                        h = int(vals[1])
                        c = N/n
                plt.annotate(s = " "+str(n)+" ("+str(h)+")["+str(c)+"]", xy = (X[i],Y[i]))
            iColor += 1
        plt.legend()           
        plt.xlabel('duration')
        plt.ylabel(metric)
        plt.title(dataset)
        #plt.show()
        filename = dataset+"_"+str(metric)+".png"
        plt.savefig("charts/"+filename)        
        plt.clf()
        plt.close()
    
    def plotSpeedupVsAccuracy(self, scores, durations, metric, dataset, datasetInfo):
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        iColor = 0      
        N = datasetInfo[dataset]['N']
        for method in scores:
            if not (("Radon" in method) or ("Averaging" in method)):
                continue  
            X = []
            Y = []
            Xerr = []
            Yerr = []        
            for key in scores[method][dataset][metric]:
                if key == CENTRAL:
                    n = N
                    h = 0
                else:
                    if not TIMED in key: 
                        n = key[0]
                        h = key[1]
                    else:
                        vals = key.replace(TIMED,'').replace('(','').replace(')','').split(',')
                        n = int(vals[0])
                        h = int(vals[1])
                score       = scores[method][dataset][metric][key]['avg']
                scoreErr    = scores[method][dataset][metric][key]['std']
                dur         = durations[method][dataset][key]['avg']['total']
                durErr      = durations[method][dataset][key]['std']['total']
                X.append(dur)
                Xerr.append(durErr)
                Y.append(score)
                Yerr.append(scoreErr)                            
            ax.scatter(X, Y, marker='o', color=COLORS[iColor], label = method)
            for i in xrange(len(X)):
                key = scores[method][dataset][metric].keys()[i]
                if key == CENTRAL:
                    n = datasetInfo[dataset]['N']
                    h = 0
                    c = 1
                else:
                    n = key[0]
                    h = key[1]
                    c = N/n
                plt.annotate(s = " "+str(n)+" ("+str(h)+")["+str(c)+"]", xy = (X[i],Y[i]))
            iColor += 1
        plt.legend()           
        plt.xlabel('duration')
        plt.ylabel(metric)
        plt.title(dataset)
        #plt.show()
        filename = "VS_"+dataset+"_"+str(metric)+".png"
        plt.savefig("charts/"+filename)        
        plt.clf()
        plt.close()
         
    def plotTheBigPictureOld(self, scores, durations, metric, dataset):        
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        iColor = 0      
        for method in scores:    
            X = []
            Y = []
            Xerr = []
            Yerr = []        
            for n in scores[method][dataset][metric]:
                for h in scores[method][dataset][metric][n]:
                    score       = scores[method][dataset][metric][n][h]['avg']
                    scoreErr    = scores[method][dataset][metric][n][h]['std']
                    dur         = durations[method][dataset][n][h]['avg']['total']
                    durErr      = durations[method][dataset][n][h]['std']['total']
                    X.append(dur)
                    Xerr.append(durErr)
                    Y.append(score)
                    Yerr.append(scoreErr)                            
            ax.errorbar(X, Y, xerr=Xerr, yerr=Yerr, ls='', marker='o', color=COLORS[iColor], label = method)
            for i in xrange(len(X)):
                plt.annotate(s = " "+str(n)+" ("+str(h)+")", xy = (X[i],Y[i]))
            iColor += 1
        plt.legend()           
        plt.xlabel('duration')
        plt.ylabel(metric)
        plt.title(dataset)
        #plt.show()
        filename = dataset+"_"+str(metric)+".png"
        plt.savefig("charts/"+filename)        
        plt.clf()
        plt.close()
#         
    def plotMetricRadonVsCentralAllDatasets(self, scores, durations, metric): 
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        methods = scores.keys()   
        central = ""
        radon = ""
        plt.plot([0., 1.1], [0., 1.1], '-', lw=1, color='black')
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        points = {}     
        heights = {}   
        for dataset in scores[methods[0]]:                
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']
            scoreRadon = -1.0
            for key in scores[radon][dataset][metric]:
                if key != CENTRAL:
                    scoreRadon = scores[radon][dataset][metric][key]['avg']
                    points[dataset+"_"+str(key)] = (scoreCentral,scoreRadon)     
                    heights[dataset+"_"+str(key)] = key[1]                                         
        X = []
        Y = []
        C = []
        for key in points:
            X.append(points[key][0])
            Y.append(points[key][1])
            C.append(heights[key])
            #plt.annotate(s = key, xy = (points[key][0],points[key][1]))
        plt.scatter(X,Y, c=C)
        #plt.legend()           
        ax.set_aspect('equal')
        plt.xlabel('central')
        plt.ylabel('radon')
        plt.xlim([0.,1.1])
        plt.ylim([0.,1.1])
        plt.title(str(metric))
        #plt.show()
        filename = str(metric)+"RadonVsCentral.png"
        plt.savefig("charts/"+filename, dpi=300)        
        plt.clf()
        plt.close()
        
    def plotDurationRadonVsCentralAllDatasets(self, scores, durations, metric): 
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        methods = scores.keys()
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        points = {}
        heights = {}
        for dataset in durations[methods[0]]:                
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']
            durationRadon = -1.0
            for key in durations[radon][dataset]:
                if key != CENTRAL:
                    durationRadon = durations[radon][dataset][key]['avg']['total']
                    points[dataset+"_"+str(key)] = (durationCentral,durationRadon)   
                    heights[dataset+"_"+str(key)] = key[1]                                        
        X = []
        Y = []
        C = []
        for key in points:
            X.append(points[key][0])
            Y.append(points[key][1])
            C.append(heights[key])
            #plt.annotate(s = key, xy = (points[key][0],points[key][1]))
        plt.scatter(X,Y, c=C)
        #plt.legend()   
        #ax.set_yscale('log')
        #ax.set_xscale('log')  
        #plt.xlim([0.,10000])
        #plt.ylim([-1.,10])
        #ax.set_aspect('equal')
        plt.xlabel('central')
        plt.ylabel('radon')
        plt.title("Duration")
        #plt.show()
        filename = "durationRadonVsCentral.png"
        plt.savefig("charts/"+filename, dpi=300)        
        plt.clf()
        plt.close()
        
    def plotMetricAndDurationRadonVsCentralAllDatasets(self, scores, durations, metric): 
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        methods = scores.keys()    
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        pointsCentral = {}
        pointsRadon = {}
        heights = {}
        for dataset in durations[methods[0]]:                
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']            
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']            
            pointsCentral[dataset] = (durationCentral,scoreCentral)     
            for key in scores[radon][dataset][metric]:
                if key != CENTRAL:
                    durationRadon = durations[radon][dataset][key]['avg']['total']                 
                    scoreRadon = scores[radon][dataset][metric][key]['avg']
                    pointsRadon[dataset+"_"+str(key)] = (durationRadon,scoreRadon) 
                    heights[dataset+"_"+str(key)] = key[1]                                        
        X_r = []
        Y_r = []
        for key in pointsRadon:
            X_r.append(pointsRadon[key][0])
            Y_r.append(pointsRadon[key][1])
        X_c = []
        Y_c = []
        for key in pointsCentral:
            X_c.append(pointsCentral[key][0])
            Y_c.append(pointsCentral[key][1])
            #plt.annotate(s = key, xy = (points[key][0],points[key][1]))
        plt.scatter(X_r,Y_r, c='blue', label='ParallelRadon')
        plt.scatter(X_c,Y_c, c='red', label='Central')
        
        for dataset in pointsCentral:
            X_d = []
            Y_d = []
            X_d.append(pointsCentral[dataset][0])
            Y_d.append(pointsCentral[dataset][1])
            for key in pointsRadon:
                if dataset in key:
                    X_d.append(pointsRadon[key][0])
                    Y_d.append(pointsRadon[key][1])
            plt.plot(X_d,Y_d, c='black', alpha=0.5)
                    
        
        plt.legend()   
        #ax.set_yscale('log')
        ax.set_xscale('log')  
        #plt.xlim([0.,10000])
        #plt.ylim([-1.,10])
        #ax.set_aspect('equal')
        plt.xlabel('time')
        plt.ylabel(str(metric))
        plt.title("Duration / Accuracy")
        #plt.show()
        filename = "durationVsACC.png"
        plt.savefig("charts/"+filename, dpi=300)        
        plt.clf()
        plt.close()
        
    def plotMetricRatioAndDurationHistogramAllDatasets(self, scores, durations, metric):
        log( "Plotting histogram: metric and duration ratio... ", newline = False)
        methods = scores.keys()   
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        
        speedups = {}
        accRatios = {}
        datasets = [d for d in scores[central].keys() if not 'synthetic' in d]
        
        maxH = 0
        for dataset in datasets:
            for key in scores[radon][dataset][metric]:
                if key[1] > maxH:
                    maxH = key[1]
        log( "max-h = ",maxH, newline = False)
        for dataset in datasets:
            speedups[dataset] = {}
            accRatios[dataset] = {}
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']
            for key in sorted(scores[radon][dataset][metric].keys(), key=lambda x: x[1]):
                h = key[1]
                scoreRadon = scores[radon][dataset][metric][key]['avg']
                durationRadon = durations[radon][dataset][key]['avg']['total']
                speedup = durationCentral / durationRadon
                accRatio = scoreRadon / scoreCentral
                speedups[dataset][h] = speedup
                accRatios[dataset][h] = accRatio
            for h in xrange(1,maxH+1):
                if h not in speedups[dataset]:
                    speedups[dataset][h] = 0.0
                if h not in accRatios[dataset]:
                    accRatios[dataset][h] = 0.0
         
        figprops = dict(figsize=(8., 8. / 1.618), dpi=300)                                          
        adjustprops = dict(left=0.1, bottom=0.1, right=0.97, top=0.93, wspace=0.2, hspace=0.1)       
        fig = plt.figure(**figprops)                                                             
        fig.subplots_adjust(**adjustprops)                                                        
        ax = fig.add_subplot(2, 1, 1)
        bx = fig.add_subplot(2, 1, 2, sharex=ax)
        
        width = 0.1
        barsAccRatios = []
        barsSpeedups = []
        minAccRatio = 1.0
        for h in xrange(1,maxH+1):
            barsAccRatios.append([])
            barsSpeedups.append([])
            for dataset in datasets:
                if accRatios[dataset][h] < minAccRatio and accRatios[dataset][h] > 0:
                    minAccRatio = accRatios[dataset][h]
                barsAccRatios[h-1].append(accRatios[dataset][h])
                barsSpeedups[h-1].append(speedups[dataset][h])
        for h in xrange(1,maxH+1):
            ax.bar(np.array(range(len(datasets)))+(h-1)*width,barsAccRatios[h-1], width, color=cm.jet(1.*h/maxH))
            bx.bar(np.array(range(len(datasets)))+(h-1)*width,barsSpeedups[h-1], width, log = True, color=cm.jet(1.*h/maxH))  
        
        ax.set_ylabel('ACC ratio')
        bx.set_ylabel('speedup')
        ax.set_ylim([minAccRatio-0.1,1.1])
    
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xticks(np.array(range(len(datasets))) + (maxH/2.)*width)
        plt.setp(ax.get_yticklabels(), size=3)
        plt.setp(bx.get_yticklabels(), size=3)
        plt.setp(bx.get_xticklabels(), size=3)
        bx.set_xticklabels(datasets, None, False, size = 8, rotation=40)
        
        #bx.set_xlim([0,len(self.m_oAnnotator.m_aDates)])
        
        #ax.legend(prop={'size':9}, borderaxespad=1.0)#bbox_to_anchor=(0.6, 0.5), ncol=1, loc=2, borderaxespad=0.)
        #bx.legend(loc=4, borderaxespad=1.0, prop={'size':9})
        
        fig.savefig("charts/"+"histogram"+str(metric)+"RatioandSpeedup.png", dpi=300)
        plt.clf()
        plt.close()
        #plt.show()
        
    def plotMetricAndDurationHistogramAllDatasets(self, scores, durations, metric):
        log( "Plotting histogram: metric and duration ratio... ", newline = False)
        methods = scores.keys()    
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        
        speedups = {}
        accsRadon = {}
        accsCentral = {}
        datasets = [d for d in scores[central].keys() if not 'synthetic' in d]
        
        maxH = 0
        for dataset in datasets:
            for key in scores[radon][dataset][metric]:
                if key[1] > maxH:
                    maxH = key[1]
        for dataset in datasets:
            speedups[dataset] = {}
            accsRadon[dataset] = {}
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']
            for key in sorted(scores[radon][dataset][metric].keys(), key=lambda x: x[1]):
                h = key[1]
                scoreRadon = scores[radon][dataset][metric][key]['avg']
                durationRadon = durations[radon][dataset][key]['avg']['total']
                speedup = durationCentral / durationRadon
                speedups[dataset][h] = speedup
                accsRadon[dataset][h] = scoreRadon
                accsCentral[dataset] = scoreCentral
            for h in xrange(1,maxH+1):
                if h not in speedups[dataset]:
                    speedups[dataset][h] = 0.0
                if h not in accsRadon[dataset]:
                    accsRadon[dataset][h] = 0.0
         
        figprops = dict(figsize=(8., 8. / 1.618), dpi=300)                                          
        adjustprops = dict(left=0.1, bottom=0.1, right=0.97, top=0.93, wspace=0.2, hspace=0.1)       
        fig = plt.figure(**figprops)                                                             
        fig.subplots_adjust(**adjustprops)                                                        
        ax = fig.add_subplot(2, 1, 1)
        bx = fig.add_subplot(2, 1, 2, sharex=ax)
        
        width = 0.2
        barsAccs = []
        barsSpeedups = []
        minAcc = 1.0
        maxAcc = 1.0
        minSpeedup = 1000000.0
        maxSpeedup = 0.0
        for h in xrange(1,maxH+1):
            barsAccs.append([])
            barsSpeedups.append([])
            for dataset in datasets:
                if accsRadon[dataset][h] < minAcc and accsRadon[dataset][h] > 0:
                    minAcc = accsRadon[dataset][h]
                if accsRadon[dataset][h] > maxAcc and accsRadon[dataset][h] > 0:
                    maxAcc = accsRadon[dataset][h]
                barsAccs[h-1].append(accsRadon[dataset][h])
                barsSpeedups[h-1].append(speedups[dataset][h])
                if speedups[dataset][h] < minSpeedup:
                    minSpeedup = speedups[dataset][h]
                if speedups[dataset][h] > maxSpeedup:
                    maxSpeedup = speedups[dataset][h]
        line = None     
        fact = 1.6   
        for i in xrange(len(datasets)):
            dataset = datasets[i]
            scoreCentral = accsCentral[dataset]
            line, = ax.plot([i*fact, fact*i+maxH*width], [scoreCentral, scoreCentral], '-', lw=1, color='black', label='central')
        for h in xrange(1,maxH+1):
            ax.bar(fact*np.array(range(len(datasets)))+(h-1)*width,barsAccs[h-1], width, color=cm.jet(1.*h/maxH), edgecolor="none")
            bx.bar(fact*np.array(range(len(datasets)))+(h-1)*width,barsSpeedups[h-1], width, log = True, color=cm.jet(1.*h/maxH), edgecolor="none")  
        
        ax.set_ylabel(metric)
        bx.set_ylabel('speedup')
        ax.set_ylim([minAcc-0.1,maxAcc + 0.1*maxAcc])
        ax.legend([line],['central'], loc='upper right', bbox_to_anchor=(1.0, 1.01), fontsize='x-small', labelspacing=0.2)        
        bx.set_ylim([1.0,maxSpeedup * 1.1])
        ax.set_xlim([0,len(datasets)+14])
        bx.set_xlim([0,len(datasets)+14])
        
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xticks(fact*np.array(range(len(datasets))) + (maxH/2.)*width)
        plt.setp(ax.get_yticklabels(), size=8)
        plt.setp(bx.get_yticklabels(), size=8)
        plt.setp(bx.get_xticklabels(), size=3)
        cleanedDatasetNames = []
        for dataset in datasets:
            if dataset == 'electricity-normalized':
                cleanedDatasetNames.append('electricity')
            elif dataset == 'Click_prediction_small':
                cleanedDatasetNames.append('Click_pred.')
            elif dataset == 'eeg-eye-state':
                cleanedDatasetNames.append('eeg-eye-st.')
            elif dataset == 'mammography':
                cleanedDatasetNames.append('mammogr.')
            elif dataset == 'skin_segmentation':
                cleanedDatasetNames.append('skin-segm.')
            elif dataset == 'MagicTelescope':
                cleanedDatasetNames.append('MagicTelesc.')                
            else:
                cleanedDatasetNames.append(dataset)
        bx.set_xticklabels(cleanedDatasetNames, None, False, size = 8, rotation=25)
        
        #bx.set_xlim([0,len(self.m_oAnnotator.m_aDates)])
        
        #ax.legend(prop={'size':9}, borderaxespad=1.0)#bbox_to_anchor=(0.6, 0.5), ncol=1, loc=2, borderaxespad=0.)
        #bx.legend(loc=4, borderaxespad=1.0, prop={'size':9})
        #plt.show()
        fig.savefig("charts/"+"histogram"+str(metric)+"andSpeedup.png", dpi=300)
        plt.clf()
        plt.close()
        log( "done.")
        #plt.show()
        
    def plotMetricAndDurationWithCentralSameTimeHistogramAllDatasets(self, scores, durations, metric):
        log( "Plotting histogram: metric and duration ratio., including score central timed.. ", newline = False)
        methods = scores.keys()    
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        
        speedups = {}
        accsRadon = {}
        accsCentral = {}
        accsCentralTimed = {}
        datasets = [d for d in scores[central].keys() if not 'synthetic' in d]
        
        maxH = 0
        for dataset in datasets:
            for key in scores[radon][dataset][metric]:
                if key[1] > maxH:
                    maxH = key[1]
        for dataset in datasets:
            speedups[dataset] = {}
            accsRadon[dataset] = {}
            accsCentralTimed[dataset] = {}
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']
            for key in sorted(scores[radon][dataset][metric].keys(), key=lambda x: x[1]):
                TIMED_PARAM = TIMED + str(key)
                h = key[1]
                scoreRadon = scores[radon][dataset][metric][key]['avg']
                durationRadon = durations[radon][dataset][key]['avg']['total']
                scoreCentralTimed = scores[central][dataset][metric][TIMED_PARAM]['avg']
                speedup = durationCentral / durationRadon
                speedups[dataset][h] = speedup
                accsRadon[dataset][h] = scoreRadon
                accsCentral[dataset] = scoreCentral
                accsCentralTimed[dataset] = scoreCentralTimed
            for h in xrange(1,maxH+1):
                if h not in speedups[dataset]:
                    speedups[dataset][h] = 0.0
                if h not in accsRadon[dataset]:
                    accsRadon[dataset][h] = 0.0
         
        figprops = dict(figsize=(8., 8. / 1.618), dpi=300)                                          
        adjustprops = dict(left=0.1, bottom=0.1, right=0.97, top=0.93, wspace=0.2, hspace=0.1)       
        fig = plt.figure(**figprops)                                                             
        fig.subplots_adjust(**adjustprops)                                                        
        ax = fig.add_subplot(2, 1, 1)
        bx = fig.add_subplot(2, 1, 2, sharex=ax)
        
        width = 0.2
        barsAccs = []
        barsSpeedups = []
        minAcc = 1.0
        maxAcc = 1.0
        minSpeedup = 1000000.0
        maxSpeedup = 0.0
        for h in xrange(1,maxH+1):
            barsAccs.append([])
            barsSpeedups.append([])
            for dataset in datasets:
                if accsRadon[dataset][h] < minAcc and accsRadon[dataset][h] > 0:
                    minAcc = accsRadon[dataset][h]
                if accsRadon[dataset][h] > maxAcc and accsRadon[dataset][h] > 0:
                    maxAcc = accsRadon[dataset][h]
                barsAccs[h-1].append(accsRadon[dataset][h])
                barsSpeedups[h-1].append(speedups[dataset][h])
                if speedups[dataset][h] < minSpeedup:
                    minSpeedup = speedups[dataset][h]
                if speedups[dataset][h] > maxSpeedup:
                    maxSpeedup = speedups[dataset][h]
        avgAccDiff = 0.0
        count = 0.0
        for dataset in datasets:
            for h in xrange(1,maxH+1):
                avgAccDiff += accsRadon[dataset][h] / accsCentralTimed[dataset]
                count += 1.0
        avgAccDiff /= count
        log( avgAccDiff)
        
        return False
        line = None     
        fact = 1.6   
        for i in xrange(len(datasets)):
            dataset = datasets[i]
            scoreCentral = accsCentral[dataset]
            scoreCentralTimed = accsCentralTimed[dataset]
            lineCentral, = ax.plot([i*fact, fact*i+maxH*width], [scoreCentral, scoreCentral], '-', lw=1, color='black', label='central')
            lineTimed, = ax.plot([i*fact, fact*i+maxH*width], [scoreCentralTimed, scoreCentralTimed], ':', lw=1, color='black', label='central(timed)')
        for h in xrange(1,maxH+1):
            ax.bar(fact*np.array(range(len(datasets)))+(h-1)*width,barsAccs[h-1], width, color=cm.jet(1.*h/maxH), edgecolor="none")
            bx.bar(fact*np.array(range(len(datasets)))+(h-1)*width,barsSpeedups[h-1], width, log = True, color=cm.jet(1.*h/maxH), edgecolor="none")  
        
        ax.set_ylabel(metric)
        bx.set_ylabel('speedup')
        ax.set_ylim([minAcc-0.1,maxAcc + 0.1*maxAcc])
        ax.legend([lineCentral, lineTimed],['sequential','equalTime'], loc='upper right', bbox_to_anchor=(1.0, 1.02), fontsize='x-small', labelspacing=0.2)        
        bx.set_ylim([1.0,maxSpeedup * 1.1])
        ax.set_xlim([0,len(datasets)+14])
        bx.set_xlim([0,len(datasets)+14])
        
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xticks(fact*np.array(range(len(datasets))) + (maxH/2.)*width)
        plt.setp(ax.get_yticklabels(), size=8)
        plt.setp(bx.get_yticklabels(), size=8)
        plt.setp(bx.get_xticklabels(), size=3)
        cleanedDatasetNames = []
        for dataset in datasets:
            if dataset == 'electricity-normalized':
                cleanedDatasetNames.append('electricity')
            elif dataset == 'Click_prediction_small':
                cleanedDatasetNames.append('Click_pred.')
            elif dataset == 'eeg-eye-state':
                cleanedDatasetNames.append('eeg-eye')
            elif dataset == 'mammography':
                cleanedDatasetNames.append('mammogr.')
            elif dataset == 'skin_segmentation':
                cleanedDatasetNames.append('skin-segm.')
            elif dataset == 'MagicTelescope':
                cleanedDatasetNames.append('MagicTelesc.')                
            else:
                cleanedDatasetNames.append(dataset)
        bx.set_xticklabels(cleanedDatasetNames, None, False, size = 8, rotation=25)
        
        #bx.set_xlim([0,len(self.m_oAnnotator.m_aDates)])
        
        #ax.legend(prop={'size':9}, borderaxespad=1.0)#bbox_to_anchor=(0.6, 0.5), ncol=1, loc=2, borderaxespad=0.)
        #bx.legend(loc=4, borderaxespad=1.0, prop={'size':9})
        #plt.show()
        fig.savefig("charts/"+"histogram"+str(metric)+"andSpeedupandCentralTimed.png", dpi=300)
        plt.clf()
        plt.close()
        log( "done.")
        #plt.show()
        
    def plotMetricAndDurationWithCentralSameTimeHistogramMaxHAllDatasets(self, scores, durations, metric):
        log( "Plotting histogram: metric and duration ratio., including score central timed for max-h.. ", newline = False)
        methods = scores.keys()    
        central = ""
        radon = ""
        for method in methods:
            if not 'RadonBoost' in method and not 'Averaging' in method:
                central = method
            if 'RadonBoost' in method:
                radon = method
        
        speedups = {}
        accsRadon = {}
        accsCentral = {}
        accsCentralTimed = {}
        datasets = [d for d in scores[central].keys() if not 'synthetic' in d]
        
        maxH = 0
        for dataset in datasets:
            for key in scores[radon][dataset][metric]:
                if key[1] > maxH:
                    maxH = key[1]
        for dataset in datasets:
            speedups[dataset] = {}
            accsRadon[dataset] = {}
            accsCentralTimed[dataset] = {}
            scoreCentral = scores[central][dataset][metric][CENTRAL]['avg']
            durationCentral = durations[central][dataset][CENTRAL]['avg']['total']
            for key in sorted(scores[radon][dataset][metric].keys(), key=lambda x: x[1]):
                TIMED_PARAM = TIMED + str(key)
                h = key[1]
                scoreRadon = scores[radon][dataset][metric][key]['avg']
                durationRadon = durations[radon][dataset][key]['avg']['total']
                scoreCentralTimed = scores[central][dataset][metric][TIMED_PARAM]['avg']
                speedup = durationCentral / durationRadon
                speedups[dataset][h] = speedup
                accsRadon[dataset][h] = scoreRadon
                accsCentral[dataset] = scoreCentral
                accsCentralTimed[dataset] = scoreCentralTimed
            for h in xrange(1,maxH+1):
                if h not in speedups[dataset]:
                    speedups[dataset][h] = 0.0
                if h not in accsRadon[dataset]:
                    accsRadon[dataset][h] = 0.0
         
        figprops = dict(figsize=(8., 8. / 1.618), dpi=300)                                          
        adjustprops = dict(left=0.1, bottom=0.1, right=0.97, top=0.93, wspace=0.2, hspace=0.1)       
        fig = plt.figure(**figprops)                                                             
        fig.subplots_adjust(**adjustprops)                                                        
        ax = fig.add_subplot(2, 1, 1)
        bx = fig.add_subplot(2, 1, 2, sharex=ax)
        
        width = 1.0
        barsAccs = []
        barsSpeedups = []
        minAcc = 1.0
        maxAcc = 1.0
        minSpeedup = 1000000.0
        maxSpeedup = 0.0        
        for dataset in datasets:
            h = max([x for x in accsRadon[dataset].keys() if accsRadon[dataset][x] > 0.0])
            if accsRadon[dataset][h] < minAcc and accsRadon[dataset][h] > 0:
                minAcc = accsRadon[dataset][h]
            if accsRadon[dataset][h] > maxAcc and accsRadon[dataset][h] > 0:
                maxAcc = accsRadon[dataset][h]
            barsAccs.append(accsRadon[dataset][h])
            barsSpeedups.append(speedups[dataset][h])
            if speedups[dataset][h] < minSpeedup:
                minSpeedup = speedups[dataset][h]
            if speedups[dataset][h] > maxSpeedup:
                maxSpeedup = speedups[dataset][h]
        line = None     
        fact = 1.6   
        for i in xrange(len(datasets)):
            dataset = datasets[i]
            scoreCentral = accsCentral[dataset]
            scoreCentralTimed = accsCentralTimed[dataset]
            lineCentral, = ax.plot([i*fact+ 0.5*width, fact*i+width+ 0.5*width], [scoreCentral, scoreCentral], '-', lw=1, color='black', label='central')
            lineTimed, = ax.plot([i*fact+ 0.5*width, fact*i+width+ 0.5*width], [scoreCentralTimed, scoreCentralTimed], ':', lw=1, color='black', label='central(timed)')
        ax.bar(fact*np.array(range(len(datasets))) + 0.5*width,barsAccs, width, color='#4CCBF5', edgecolor="none")
        bx.bar(fact*np.array(range(len(datasets))) + 0.5*width,barsSpeedups, width, log = True, color='b', edgecolor="none")  
        
        ax.set_ylabel(metric)
        bx.set_ylabel('speedup')
        ax.set_ylim([minAcc-0.1,maxAcc + 0.1*maxAcc])
        ax.legend([lineCentral, lineTimed],['sequential','equalTime'], loc='upper right', bbox_to_anchor=(1.0, 1.02), fontsize='x-small', labelspacing=0.2)        
        bx.set_ylim([1.0,maxSpeedup * 1.1])
        ax.set_xlim([0,len(datasets)+14])
        bx.set_xlim([0,len(datasets)+14])
        
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xticks(fact*np.array(range(len(datasets)+1)) + width)
        plt.setp(ax.get_yticklabels(), size=8)
        plt.setp(bx.get_yticklabels(), size=8)
        plt.setp(bx.get_xticklabels(), size=3)
        cleanedDatasetNames = []
        for dataset in datasets:
            if dataset == 'electricity-normalized':
                cleanedDatasetNames.append('electricity')
            elif dataset == 'Click_prediction_small':
                cleanedDatasetNames.append('Click_pred.')
            elif dataset == 'eeg-eye-state':
                cleanedDatasetNames.append('eeg-eye')
            elif dataset == 'mammography':
                cleanedDatasetNames.append('mammogr.')
            elif dataset == 'skin_segmentation':
                cleanedDatasetNames.append('skin-segm.')
            elif dataset == 'MagicTelescope':
                cleanedDatasetNames.append('MagicTelesc.')                
            else:
                cleanedDatasetNames.append(dataset)
        bx.set_xticklabels(cleanedDatasetNames, None, False, size = 8, rotation=25)
        
        #bx.set_xlim([0,len(self.m_oAnnotator.m_aDates)])
        
        #ax.legend(prop={'size':9}, borderaxespad=1.0)#bbox_to_anchor=(0.6, 0.5), ncol=1, loc=2, borderaxespad=0.)
        #bx.legend(loc=4, borderaxespad=1.0, prop={'size':9})
        #plt.show()
        fig.savefig("charts/"+"histogram"+str(metric)+"andSpeedupandCentralTimedMaxH.png", dpi=300)
        plt.clf()
        plt.close()
        log( "done.")
        #plt.show()