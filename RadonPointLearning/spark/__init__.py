import findspark
import pyspark
import zipfile
import os
import numpy as np
from datetime import datetime
from learner import Learner
from framework.logger import log

findspark.init()
sparkConf = pyspark.conf.SparkConf().set("spark.driver.memory", "16g").set("spark.driver.maxResultSize","4g").set("spark.akka.frameSize", "2047").set("spark.executor.memory", "8g")
globalSparkContext = pyspark.SparkContext(conf = sparkConf)

print "SPARK configuration: ", globalSparkContext._conf.getAll()

def getRootPath():   
    count = 0 
    rootPath = os.getcwd()
    while not rootPath.endswith('RadonPointLearning') and count < 100:
        rootPath = os.path.abspath(os.path.join(rootPath, os.pardir))
        count += 1
    if not rootPath.endswith('RadonPointLearning'):
        print "Error finding root path of project with project name \"RadonPointLearning\""
    return rootPath.replace("\\","/")

def ziplib(libpath, filename = "library"):    
    zippath = filename + '.zip'
    zf = zipfile.PyZipFile(zippath, mode='w')
    try:        
        zf.writepy(libpath)
        return zippath     
    finally:
        zf.close()

rootPath = getRootPath()
if "local[*]" not in globalSparkContext.master: #PySpark is running on a cluster and libraries need to be provided to the spark workers
    for lib in ['framework', 'learner', 'data', 'radonPoints']:
        libpath = rootPath + "/" + lib
        zip_path = ziplib(libpath, lib)
        print zip_path           
        globalSparkContext.addPyFile(zip_path)
        os.remove(zip_path)

class SparkLearner(Learner):
    def __init__(self):
        self.model = np.zeros(1)
        self.isSpark = True
        self.identifier = "GenericSPARKLearner (please replace)"
    
    def getInitParams(self):
        return {}
    
    def determineSampleSize(self, X):
        return X.shape[0]
        
    def train(self, X, y):
        start = datetime.now()
        self.model = np.zeros(X.shape[1])
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
    
    def predict(self, X):
        return np.zeros(X.shape[0])
    
    def getModel(self):
        return self.model
    
    def setModel(self, model):
        self.model = model
        
    def __str__(self):
        return self.identifier