# README #

This is a quick guide on how to use the experiment framework and the parallel Radon machine.

### Contents ###

* Get the code running
* Run an experiment
* Use the parallel Radon machine independently

### Get the code running ###

* Please ensure that you have the following software installed:

1. Spark with PySpark and hadoop
2. python with numpy, scipy, sklearn, findspark, pandas (and all necessary dependencies. If I forgot a package in this list, please let me know.)

* Download the entire folder "RadonPointLearning". Now, you're good to go.
* To test if the software is running, go to /experiments/SPARK/sparkDatasets and execute exp.py (e.g., by typing python exp.py in the console)
* Now an experiment comparing the parallel Radon machine against Spark MLlib parallel learners is executed on a number of classification datasets

### Run an experiment ###

* As you have seen above, an experiment is run by simply executing an experiment script, e.g., /experiments/SPARK/sparkDatasets/exp.py
* each experiment script specifies a list of learners, metrices, parallelisation methods and datasets.
* then it creates an Experiment-Object (ParallelRadonExperiment, or SparkParallelRadonExperiment) with these parameters
* the experiment is started calling the run()-method.

### Use the parallel Radon machine independently ###

* There are two implementations of the parallel Radon Machine: radonPoints.radnonBoosting.RadonBoost and spark.radonBoosting.RadonBoost.

1. Use radonPoints.radnonBoosting.RadonBoost for a sequential version of the algorithm entirely in python that is good for running in controlled, simulated environments. This class gets the whole training dataset as two numpy arrays X,y.
2. Use spark.radonBoosting.RadonBoost for a parallel implementation of the algorithm on Spark using PySpark. This class gets the whole training set as an RDD of LabeledPoints.

* The RadonBoost class takes as initialization parameter an instance of the sklearn machine learning algorithm to be parallelised. You can use any algorithm that has a linear model (and thus the coef_ attribute). 

### Known issues ###

The implementation of the learning framework is not perfect, and there are some issues we experienced with it, which I will list below. If you experience other problems, please let me know. I'll try to fix all issues as soon as possible.

* Stratified sampling on Spark is slow, and sometimes produces wrong results.
* When executing the Spark algorithm on a real cluster, reading files from a shared filesystem does not work. For that, please upload the files to a HDFS in a folder called "sparkDatasets" in your home folder.
* Applying base learners to small samples of the data is unusually slow. This seems to be a problem with PySpark, since it needs to serialize the data from the RDD in order to feed it to the python learning algorithm. I'm currently working on a Scala implementation of the parallel Radon machine which will solve this issue, and probably some of the others as well.

### License ###
Copyright 2016

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.